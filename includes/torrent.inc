<?php

/**
 * @file torrent.inc
 * Provides .torrent file read/write support.
 */


ctools_include('bencode', 'opentracker');

/**
 * Create a torrent file.
 *
 * @param $announce
 *   The announce URL.
 * @param $input_file
 *   The full path of the file for which a torrent should be created.
 * @param $torrent_file
 *   The full path of the destination .torrent file.
 * @param $publisher
 *   The name of the organization that publishes this torrent.
 * @param $publisher_url
 *   The URL of the organization that publishes this torrent.
 * @param $comment
 * @param (optional) $creation_date
 *   The creation date stored inside the .torrent file. Defaults to the
 *   current time.
 */
function opentracker_torrent_create($announce, $input_file, $torrent_file, $publisher, $publisher_url, $comment, $creation_date = NULL) {
  if (!isset($creation_date)) {
    $creation_date = time();
  }

  $length = filesize($input_file);

  // 512 KB is the default maximum piece length
  //
  // From http://wiki.theory.org/BitTorrentSpecification:
  //  "Current best-practice is to keep the piece size to 512KB or less, for
  //   torrents around 8-10GB, even if that results in a larger .torrent file.
  //   This results in a more efficient swarm for sharing files.""
  $piece_length = 524288;

  // Keep making the piece length:
  // - smaller until there is >1 piece, or;
  // - larger until there are <1500 pieces
  // From http://wiki.vuze.com/w/Torrent_Piece_Size:
  //  "All in all, a torrent should have around 1000-1500 pieces, to get a
  //   reasonably small torrent file and an efficient client and swarm
  //   download."
  while (ceil($length / $piece_length) == 1) {
    $piece_length /= 2;
  }
  while (ceil($length / $piece_length) > 1500) {
    $piece_length *= 2;
  }

  // Build pieces.
  if (($fp = fopen($input_file, 'r')) == FALSE) {
    return FALSE;
  }
  $pieces = '';
  while (!feof($fp)) {
    $pieces .= opentracker_hex2bin(sha1(fread($fp, $piece_length)));
  }
  fclose($fp);

  // Build the torrent data structure.
  $torrent = array(
    'announce'      => $announce,
    'creation date' => $creation_date,
    'created by'    => $publisher,
    'publisher'     => $publisher,
    'publisher-url' => $publisher_url,
    'comment'       => $comment,
    'info' => array(
      'length'       => $length,
      'name'         => basename($input_file),
      'piece length' => $piece_length,
      'pieces'       => $pieces,
      'md5sum'       => md5_file($input_file),
    ),
  );

  // Save the .torrent file.
  file_put_contents($torrent_file, opentracker_bencode($torrent));

  return $torrent;
}

/**
 * Calculate the info_hash for the given torrent metadata.
 *
 * @param $torrent_metadata
 *   The metadata for a torrent. Data returned by opentracker_bdecode_file()
 *   can be passed as a parameter here.
 * @return
 *   The corresponding info_hash, as an ASCII string (not as a binary string,
 *   use opentracker_hex2bin() on the return value to get a binary string).
 */
function opentracker_torrent_infohash($torrent_metadata) {
  return strtoupper(sha1(opentracker_bencode($torrent_metadata['info'])));
}
