<?php

/**
 * @file bencode.inc
 * Provides Bencode encoding support.
 *
 * All functionality in this file was borrowed from
 * http://drupal.org/project/bittorrent. Gradually, these functions may be
 * improved.
 */


/**
 * Bencode data.
 *
 * @param $data
 *   Acceptable values include: integers, strings, arrays, and associative
 *   arrays. Note that associative arrays are encoded into dictionaries while
 *   non-associative arrays are encoded into lists.
 *
 * @param $data
 *    The data to bencode.
 * @return
 *    The bencoded $data.
 */
function opentracker_bencode($data) {
  if (is_array($data)) {
    $numeric = TRUE;
    foreach(array_keys($data) as $key) {
      if (!is_numeric($key)) {
        $numeric = FALSE;
        break;
      }
    }

    if (!$numeric) {
      $out = 'd';
      ksort($data);
      foreach ($data as $key => $value) {
        $out .= opentracker_bencode($key);
        $out .= opentracker_bencode($value);
      }
      $out .= 'e';
    }
    else {
      $out = 'l';
      ksort($data);
      foreach ($data as $key => $value) {
        $out .= opentracker_bencode($value);
      }
      $out .= 'e';
    }

    return $out;
  }
  elseif (preg_match('/^(\+|\-)?\d+$/', $data)) {
    return 'i' . $data . 'e';
  }
  else {
    return strlen($data) . ':' . $data;
  }
}

/**
 * Bencode data and write the result to a file.
 *
 * @param $data
 *   The data to bencode. @see opentracker_bencode()
 * @param $file
 *   Full path to the file where the bencoded data should be written.
 * @return
 *   The number of bytes written, or FALSE.
 */
function bencode_file($data, $file) {
  return file_put_contents($file, opentracker_bencode($data));
}

/**
 * Decodes bencoded data.
 * @TODO: needs to be cleaned up
 *
 * @param $metadata
 *    The data to be decoded
 * @return The decoded data in the following structure (or NULL if error):
 *    array(
 *      ['type'] => type,              // This can be integer, string, list, dictionary
 *      ['value'] => value,            // This is the decoded value
 *      ['strlen'] => strlen,          // This is the length of the bencoded string
 *      ['string'] => bstring,         // This is the bencoded string
 *    )
 */
function opentracker_bdecode($metadata) {
  if (preg_match('/^(\d+):/', $metadata, $matches)) {
    $length = $matches[1];
    $string_start = strlen($length) + 1;
    $value = substr($metadata, $string_start, $length);
    $bencoded = substr($metadata, 0, $string_start + $length);
    if (strlen($value) != $length) {
      return;
    }
    return array('type' => "string", 'value' => $value, 'strlen' => strlen($bencoded), 'string' => $bencoded);
  }

  if (preg_match('/^i(\d+)e/', $metadata, $matches)) {
    $value = $matches[1];
    $bencoded = "i" . $value . "e";
    if ($value === "-0") {
      return;
    }
    if ($v[0] == "0" && strlen($v) != 1) {
      return;
    }
    return array('type' => "integer", 'value' => $value, 'strlen' => strlen($bencoded), 'string' => $bencoded);
  }

  switch ($metadata[0]) {
    case "l":
      return _opentracker_bdecode_list($metadata);

    case "d":
      return _opentracker_bdecode_dictionary($metadata);

    default:
      return;
  }
}

/**
 * A friendlier interface for opentracker_bdecode(). This function returns a cleaner structure modelled after the torrent specification (or NULL if error).
 * @TODO: needs to be cleaned up
 *
 * @param $file
 *    The path to the .torrent file to decode
 * @return The structure containing all of the bencoded data. Example:
 *    array(
 *      'announce' => 'announce url',
 *      'creation date' => 'creation date',
 *      'info' => array(
 *        'files' => 'files list',
 *        'name' => 'torrent name',
 *        'piece length' => 'length of each piece',
 *        'pieces' => 'The piece hashes',
 *      ),
 *    )
 *
 * The advantage of this structure is that should it be run through bencode() the complete contents of the .torrent will be returned
 */
function opentracker_bdecode_file($file) {
  if (file_exists($file)){
    $metadata = file_get_contents($file);
    $parsed = _opentracker_bdecode_strip_excess(opentracker_bdecode($metadata));
    return $parsed;
  }
  else {
    return FALSE;
  }
}


//----------------------------------------------------------------------------
// Private functions.

/**
 * Used to decode bencoded lists
 * @TODO: needs to be cleaned up
 *
 * @param $metadata
 *    The bencoded list
 * @return An array containing the contents of the list or NULL if error.
 */
function _opentracker_bdecode_list($metadata) {
  if ($metadata[0] != "l") {
    return;
  }

  $length = strlen($metadata);
  $position = 1;
  $value = array();
  $bencoded = "l";

  for (;;) {
    if ($position >= $length) {
      return;
    }
    if ($metadata[$position] == "e") {
      break;
    }
    $return = opentracker_bdecode(substr($metadata, $position));
    if (!isset($return) || !is_array($return)) {
      return;
    }
    $value[] = $return;
    $position += $return["strlen"];
    $bencoded .= $return["string"];
  }

  $bencoded .= "e";
  return array('type' => "list", 'value' => $value, 'strlen' => strlen($bencoded), 'string' => $bencoded);
}

/**
 * Used to decode bencoded dictionaries
 * @TODO: needs to be cleaned up
 *
 * @param $metadata
 *    The bencoded dctionary
 * @return An associative array containing the keys and values of the dictionary or NULL if error.
 */
function _opentracker_bdecode_dictionary($metadata) {
  if ($metadata[0] != "d") {
    return;
  }
  $length = strlen($metadata);
  $position1 = 1;
  $value = array();
  $bencoded = "d";
  for (;;) {
    if ($position1 >= $length) {
      return;
    }
    if ($metadata[$position1] == "e") {
      break;
    }
    $return = opentracker_bdecode(substr($metadata, $position1));
    if (!isset($return) || !is_array($return) || $return["type"] != "string") {
      return;
    }
    $position2 = $return["value"];
    $position1 += $return["strlen"];
    $bencoded .= $return["string"];
    if ($position1 >= $length) {
      return;
    }
    $return = opentracker_bdecode(substr($metadata, $position1));
    if (!isset($return) || !is_array($return)) {
      return;
    }
    $value[$position2] = $return;
    $position1 += $return["strlen"];
    $bencoded .= $return["string"];
  }
  $bencoded .= "e";
  return array('type' => "dictionary", 'value' => $value, 'strlen' => strlen($bencoded), 'string' => $bencoded);
}

/**
 * Strips the unnescessary values from the bdecode() result.
 *
 * @param $bparsed
 *   The array of data returned by bdecode()
 * @return The modified structure without the 'value' field (or NULL if error). Example:
 *    array(
 *      'announce' => 'announce url',
 *      'creation date' => 'creation date',
 *      'info' => array(
 *        'files' => 'files list',
 *      'name' => 'torrent name',
 *        'piece length' => 'length of each piece',
 *        'pieces' => 'The piece hashes',
 *      ),
 *    )
 */
function _opentracker_bdecode_strip_excess($bparsed) {
  if (is_array($bparsed)) {
    foreach($bparsed as $key => $value) {
      if (is_array($value)) {
        if (array_key_exists('value', $value)) {
          $bparsed[$key] = _opentracker_bdecode_strip_excess($value['value']);
        }
        else {
          $bparsed[$key] = _opentracker_bdecode_strip_excess($value);
        }
      }
    }
  }

  if (is_array($bparsed) && array_key_exists('value', $bparsed)) {
    return $bparsed['value'];
  }
  else {
    return $bparsed;
  }
}

/**
 * Encode an ASCII string containing hexadecimal data to a binary string.
 */
function opentracker_hex2bin($string) {
  return pack('H*', $string);
}

/**
 * Decode a binary string to an ASCII string containing hexadecimal data.
 */
function opentracker_bin2hex($string) {
  return bin2hex($string);
}
