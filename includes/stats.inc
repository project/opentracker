<?php

/**
 * @file stats.inc
 * Parses statistics provided by OpenTracker.
 */


ctools_include('bencode', 'opentracker');

/**
 * Statistics provided: (hash, seeders, leechers, downloads).
 *
 * @param $info_hashes
 *   An optional array of info_hashes (hexadecimal strings). This will limit
 *   the returned statistics to the torrents that correspond to these
 *   info_hashes.
 * @return
 *   The requested statistics, keyed by info_hash.
 * 
 * TASK_FULLSCRAPE in OpenTracker's C code.
 *
 * Note: can be used for *any* tracker, since it parses according to the
 * BitTorrent /scrape convention.
 */
function opentracker_stats_fullscrape($info_hashes = NULL) {
  $url = variable_get(OPENTRACKER_URL_VARIABLE, OPENTRACKER_URL_DEFAULT) . '/scrape'; // Alternatively: /stats?mode=tpbs&format=ben
  if (is_array($info_hashes)) {
    $info_hashes = array_map('opentracker_hex2bin', $info_hashes);
    $info_hashes = array_map('urlencode', $info_hashes);
    $url .= '?info_hash=' . implode('&info_hash=', $info_hashes);
  }
  $result = drupal_http_request($url);

  $parsed = _opentracker_bdecode_strip_excess(opentracker_bdecode($result->data));

  $torrents = array();
  foreach ($parsed['files'] as $info_hash => $metadata) {
    $torrents[strtoupper(opentracker_bin2hex($info_hash))] = array(
      'seeders'   => $metadata['complete'],
      'leechers'  => $metadata['incomplete'],
      'downloads' => $metadata['downloaded'],
    );
  }

  return $torrents;
}

/**
 * Statistics provided: (hash, seeders, leechers).
 *
 * TASK_FULLSCRAPE_TPB_ASCII in OpenTracker's C code.
 */
function opentracker_stats_fullscrape_ascii() {
  $url = variable_get(OPENTRACKER_URL_VARIABLE, OPENTRACKER_URL_DEFAULT) . '/stats';  
  $result = drupal_http_request(url($url, array('query' => array('mode' => 'tpbs', 'format' => 'txt'))));

  $lines = explode("\n", $result->data);
  array_pop($lines);

  $torrents = array();
  foreach ($lines as $line) {
    list($info_hash, $seeders, $leechers) = explode(':', $line);
    $torrents[$info_hash] = array(
      'seeders'  => $seeders,
      'leechers' => $leechers,
    );
  }

  return $torrents;
}

/**
 * Statistics provided: (hash, last activity, downloads).
 *
 * TASK_FULLSCRAPE_TRACKERSTATE in OpenTracker's C code.
 */
function opentracker_stats_statedump() {
  $url = variable_get(OPENTRACKER_URL_VARIABLE, OPENTRACKER_URL_DEFAULT) . '/stats';
  $result = drupal_http_request(url($url, array('query' => array('mode' => 'statedump'))));

  $lines = explode("\n", $result->data);
  array_pop($lines);

  $torrents = array();
  foreach ($lines as $line) {
    list($info_hash, $unix_timestamp_in_minutes, $downloads) = explode(':', $line);
    $torrents[$info_hash] = array(
      'last activity' => $unix_timestamp_in_minutes * 60,
      'downloads'     => $downloads
    );
  }

  return $torrents;
}

/**
 * Extract as much information as possible from the statistics that
 * OpenTracker provides.
 *
 * Statistics provided: (hash, seeders, leechers, last activity, downloads).
 *
 * This uses both the TASK_FULLSCRAPE and TASK_FULLSCRAPE_TRACKERSTATE stats.
 */
function opentracker_stats_combined() {
  $torrents = opentracker_stats_fullscrape();
  $torrents_state = opentracker_stats_statedump();

  // Merge the information.
  foreach ($torrents_state as $info_hash => $metadata) {
    $torrents[$info_hash]['last activity'] = $torrents_state[$info_hash]['last activity'];
  }

  return $torrents;
}

/**
 * Parse the MRTG stats pages.
 *
 * @param $mode
 *   Valid values are: peer, conn, scrp, udp4, tcp4, busy, torr, fscr,
 *   completed, syncs.
 *   Parsing has only been implemented for peer, coonn, completed.
 * @return
 *   An array with statistics.
 */
function opentracker_stats_mrtg($mode) {
  $url = variable_get(OPENTRACKER_URL_VARIABLE, OPENTRACKER_URL_DEFAULT) . '/stats';
  $result = drupal_http_request(url($url, array('query' => array('mode' => $mode))));

  $lines = explode("\n", $result->data);
  array_pop($lines);

  $stats = array();
  switch ($mode) {
    case 'peer':
      preg_match('/opentracker serving (\d{1,10}) torrents/', $lines[2], $matches);    
      $stats['peers']    = intval($lines[0]);
      $stats['seeders']  = intval($lines[1]);
      $stats['leechers'] = $peers - $seeders;
      $stats['torrents'] = intval($matches[1]);
      break;
    case 'conn':
      preg_match('/(\d{1,20}) seconds .*/', $lines[2], $matches);
      $stats['requests']  = intval($lines[0]);
      $stats['announces'] = intval($lines[1]);
      $stats['uptime']    = intval($matches[1]);
      break;
    case 'completed':
      preg_match('/(\d{1,20}) seconds .*/', $lines[2], $matches);
      $stats['downloads'] = intval($lines[0]);
      $stats['uptime']    = intval($matches[1]);
      break;
  }
  
  return $stats;
}
