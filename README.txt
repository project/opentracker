
Description
-----------
This module is intended as an API module, to simplify integration of a Drupal
web site with an OpenTracker [1] BitTorrent tracker. It allows you to create
.torrent files, and parse the statistics provided by the OpenTracker instance.

This module does *not* provide a UI. It's an API module only.

[1] http://erdgeist.org/arts/software/opentracker/


Notes
-----
Only one variable is used: OPENTRACKER_URL_VARIABLE. Set it as follows:
  variable_set(OPENTRACKER_URL_VARIABLE, "http://yourtracker.com:6969")
This variable defaults to "http://localhost:6969", to facilitate localhost
testing.

All other settings are passed via API calls.

An example of how to use the API:
  variable_set(OPENTRACKER_URL_VARIABLE, 'http://tracker.driverpacks.net:6969');
  ctools_include('stats', 'opentracker');
  var_dump(opentracker_stats_fullscrape());
This sets the URL for the OpenTracker instance you want to query. Next, the
'stats' functionality of this module is included. Finally, a fullscrape is
performed, and the results are dumped.

Another example:
  ctools_include('bencode', 'opentracker');
  var_dump(opentracker_bdecode_file("some-torrent.torrent"));
This parses the given torrent file (i.e. "some-torrent.torrent") and dumps all
metadata stored inside.


Author
------
Wim Leers ~ http://wimleers.com/

Developed for http://driverpacks.net/
